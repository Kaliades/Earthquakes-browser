package com.kulinski_simon.apiapp

import android.support.test.InstrumentationRegistry
import com.kulinski_simon.apiapp.data.EarthquakeRepositoryImpl
import com.kulinski_simon.apiapp.data.api.EarthquakeApiAdapter
import com.kulinski_simon.apiapp.data.realm_db.RealmDao
import com.kulinski_simon.apiapp.domain.domain_model.Earthquake
import io.realm.Realm
import io.realm.RealmConfiguration
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class EarthquakeRepositoryTest {

    init {
        val realmConfig = RealmConfiguration.Builder()
            .inMemory()
            .name("test_db")
            .build()
        Realm.setDefaultConfiguration(realmConfig)
        Realm.init(InstrumentationRegistry.getContext())
    }

    lateinit var realm: Realm
    @Before
    fun openRealm() {
        realm = Realm.getDefaultInstance()
    }

    @After
    fun closeRealm() {
        realm.close()
    }

    private val repository = EarthquakeRepositoryImpl(RealmDao, EarthquakeApiAdapter)


    @Test
    fun getEarthquakeFromRealm() {
        val item = mockDomain("a21")
        repository.saveEarthquake(item)
        val result = repository.getEarthquakeById("a21").test().values()[0]
        Assert.assertEquals("a21", result.id)
    }

    @Test
    fun getEarthquakesCollectionFromRealm() {
        val item1 = mockDomain("a21")
        val item2 = mockDomain("a22")
        repository.saveEarthquake(item1)
        repository.saveEarthquake(item2)
        val result = repository.getListOfAllEarthquakeFromDb().test().values()[0]
        Assert.assertEquals(2, result.size)
    }

    @Test
    fun deleteFromRealm() {
        val item1 = mockDomain("a221")
        repository.saveEarthquake(item1)
        repository.deleteEarthquake("a221")
        val result = repository.getListOfAllEarthquakeFromDb().test().values()[0]
        Assert.assertEquals(0, result.size)
    }

    @Test
    fun getEarthquakeFromApi() {
        val result =
            repository.getListOfEarthquakeFromApi("query?format=geojson&starttime=2014-01-01&endtime=2014-01-02&minmagnitude=6").test().values()[0]
        Assert.assertEquals(1, result.size)
    }

    private fun mockDomain(id: String) = Earthquake(
        id, 2.0, "text", 1, 1, "text", "text", 1, "text", "text", "text", "text",
        listOf(1.0, 2.0, 3.0)
    )

}