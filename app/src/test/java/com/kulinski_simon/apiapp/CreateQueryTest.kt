package com.kulinski_simon.apiapp

import com.kulinski_simon.apiapp.data.QueryApiBuilderImpl
import com.kulinski_simon.apiapp.data.ValidatorRequestImpl
import com.kulinski_simon.apiapp.domain.domain_model.Request
import com.kulinski_simon.apiapp.domain.usecases.CreateQuery
import org.junit.Assert
import org.junit.Test

class CreateQueryTest{

    private val createQuery = CreateQuery(ValidatorRequestImpl(), QueryApiBuilderImpl())

    @Test
    fun validQuery() {
        val request = Request.Builder()
            .startData("2015-05-05")
            .ednData("2016-05-05")
            .build()
        val result = createQuery.execute(request)
        Assert.assertEquals(result.value, true, result.successful)
    }

    @Test
    fun invalidQuery() {
        val request = Request.Builder()
            .startData("2015-05-05")
            .ednData("2014-06-07")
            .build()
        val result = createQuery.execute(request)
        Assert.assertEquals(result.value, false, result.successful)
    }


}