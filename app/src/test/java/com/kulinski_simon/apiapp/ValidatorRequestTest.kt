package com.kulinski_simon.apiapp

import com.kulinski_simon.apiapp.data.ValidatorRequestImpl
import com.kulinski_simon.apiapp.domain.domain_model.Request
import com.kulinski_simon.apiapp.domain.repository.ValidatorRequest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class ValidatorRequestTest {

    private val validator = ValidatorRequestImpl()
    private var request = Request.Builder().build()

    private fun test(result: ValidatorRequest.Result, expected: Boolean) {
        Assert.assertEquals(result.error, expected, result.isValid)
    }

    @Test
    fun incorrectData() {
        request.startData = "2015-01-03"
        request.endData = "2014-05-02"
        test(validator.checkValidity(request), false)
        request.startData = "2015-01-01"
        request.endData = "2014-05-02"
        test(validator.checkValidity(request), false)
    }

    @Test
    fun incorrectYearMonthAndDayTheSame() {
        request.startData = "2015-05-05"
        request.endData = "2014-05-05"
        test(validator.checkValidity(request), false)
    }

    @Test
    fun incorrectMonthYearAndDayTheSame() {
        request.startData = "2015-05-05"
        request.endData = "2014-05-05"
        test(validator.checkValidity(request), false)
    }

    @Test
    fun incorrectDayMonthAndYearTheSame() {
        request.startData = "2014-04-30"
        request.endData = "2014-04-01"
        test(validator.checkValidity(request), false)
    }

    @Test
    fun correctData() {
        request.startData = "2014-02-04"
        request.endData = "2016-06-08"
        test(validator.checkValidity(request), true)
    }

    @Test
    fun correctDataDifferentYears() {
        request.startData = "2014-06-08"
        request.endData = "2016-06-08"
        test(validator.checkValidity(request), true)
    }

    @Test
    fun correctDataDifferentMonths() {
        request.startData = "2014-06-08"
        request.endData = "2014-09-08"
        test(validator.checkValidity(request), true)
    }

    @Test
    fun correctDataDifferentDays() {
        request.startData = "2016-06-01"
        request.endData = "2016-06-08"
        test(validator.checkValidity(request), true)
    }

    @Test
    fun incorrectMinMagnitude() {
        request.startData = "2014-02-04"
        request.endData = "2016-06-08"
        request.minMagnitude = "2-01"
        test(validator.checkValidity(request), false)
        request.minMagnitude = "2.01"
        test(validator.checkValidity(request), false)
        request.minMagnitude = "2,11"
        test(validator.checkValidity(request), false)
    }

    @Test
    fun correctMinMagnitude() {
        request.startData = "2014-02-04"
        request.endData = "2016-06-08"
        request.minMagnitude = "2.1"
        test(validator.checkValidity(request), true)
    }


}