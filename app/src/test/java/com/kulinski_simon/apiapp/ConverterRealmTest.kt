package com.kulinski_simon.apiapp

import com.kulinski_simon.apiapp.data.converters.ConverterRealmModelDomainModel
import com.kulinski_simon.apiapp.data.model.realm.RealmEarthquake
import com.kulinski_simon.apiapp.domain.domain_model.Earthquake
import io.realm.RealmList
import org.junit.Assert
import org.junit.Test

class ConverterRealmTest {

    private val converter = ConverterRealmModelDomainModel


    @Test
    fun convertRealmToDomainModel() {
        val realm = mockRealm()
        val result = compere(realm, converter.dbModelToDomainModel(realm))
        Assert.assertEquals(result.error, true, result.isEqual)
    }

    @Test
    fun convertDomainToRealm() {
        val domain = mockDomain()
        val result = compere( converter.domainModelToDbModel(domain), domain)
        Assert.assertEquals(result.error, true, result.isEqual)
    }


    private fun compere(realm: RealmEarthquake, domain: Earthquake) = when {
        domain.id != realm.id -> CompereResult.notEqual("Wrong id")
        domain.alert != realm.alert -> CompereResult.notEqual("Wrong alert")
        domain.dmin != realm.dmin -> CompereResult.notEqual("Wrong dmin")
        domain.mag != realm.mag -> CompereResult.notEqual("Wrong magnitude")
        domain.place != realm.place -> CompereResult.notEqual("Wrong place")
        domain.status != realm.status -> CompereResult.notEqual("Wrong status")
        domain.time != realm.time -> CompereResult.notEqual("Wrong time")
        domain.update != realm.update -> CompereResult.notEqual("Wrong update")
        domain.type != realm.type -> CompereResult.notEqual("Wrong type")
        domain.url != realm.url -> CompereResult.notEqual("Wrong  url")
        domain.tsunami != realm.tsunami -> CompereResult.notEqual("Wrong tsunami")
        !compereCoordinate(
            domain.coordinates,
            realm.coordinates!!.toList()
        ) -> CompereResult.notEqual("Wrong coordinate")
        else -> CompereResult.equal()
    }

    private fun compereCoordinate(domainCoordinate: List<Double>, realmCoordinate: List<Double>): Boolean {
        var isEqual = true
        domainCoordinate.indices.forEach {
            if (domainCoordinate[it] != realmCoordinate[it]) isEqual = false
        }
        return isEqual
    }


    private fun mockRealm(): RealmEarthquake {
        val realm = RealmEarthquake()
        realm.id = "1"
        realm.alert = "text"
        realm.dmin = "text"
        realm.mag = 2.0
        realm.message = "text"
        realm.place = "text"
        realm.status = "text"
        realm.time = 1
        realm.update = 1
        realm.tsunami = 1
        realm.title = "text"
        realm.url = "text"
        realm.type = "text"
        realm.coordinates = RealmList(1.0, 2.0, 3.0)
        return realm
    }

    private fun mockDomain() = Earthquake(
        "1", 2.0, "text", 1, 1, "text", "text", 1, "text", "text", "text", "text",
        listOf(1.0, 2.0, 3.0)
    )

    class CompereResult private constructor(val error: String, val isEqual: Boolean) {
        companion object {
            fun notEqual(error: String) = CompereResult(error, false)
            fun equal() = CompereResult("", true)
        }
    }

}