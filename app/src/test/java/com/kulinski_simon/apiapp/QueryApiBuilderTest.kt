package com.kulinski_simon.apiapp

import com.kulinski_simon.apiapp.data.QueryApiBuilderImpl
import com.kulinski_simon.apiapp.domain.domain_model.Request
import org.junit.Assert
import org.junit.Test

class QueryApiBuilderTest {

    private val queryBuilder = QueryApiBuilderImpl()


    @Test
    fun onlyData() {
        val request = Request.Builder()
            .startData("2015-05-05")
            .ednData("2016-05-05")
            .build()
        val expectedResult = "query?format=geojson&starttime=2015-05-05&endtime=2016-05-05"
        Assert.assertEquals(expectedResult, queryBuilder.build(request = request))
    }

    @Test
    fun dataWithMinMagnitude() {
        val request = Request.Builder()
            .startData("2015-05-05")
            .ednData("2016-05-05")
            .minMagnitude("2.0")
            .build()
        val expectedResult = "query?format=geojson&starttime=2015-05-05&endtime=2016-05-05&minmagnitude=2.0"
        Assert.assertEquals(expectedResult, queryBuilder.build(request = request))
    }

    @Test
    fun dataWithAlertLevel() {
        val request = Request.Builder()
            .startData("2015-05-05")
            .ednData("2016-05-05")
            .alertLevel("green")
            .build()
        val expectedResult = "query?format=geojson&starttime=2015-05-05&endtime=2016-05-05&alertlevel=green"
        Assert.assertEquals(expectedResult, queryBuilder.build(request = request))
    }

    @Test
    fun dataWithAlertLevelAndMinMagnitude() {
        val request = Request.Builder()
            .startData("2015-05-05")
            .ednData("2016-05-05")
            .minMagnitude("2.0")
            .alertLevel("green")
            .build()
        val expectedResult =
            "query?format=geojson&starttime=2015-05-05&endtime=2016-05-05&minmagnitude=2.0&alertlevel=green"
        Assert.assertEquals(expectedResult, queryBuilder.build(request = request))
    }
}