package com.kulinski_simon.apiapp

import android.app.Application
import com.kulinski_simon.apiapp.di.Modules
import io.realm.Realm
import io.realm.RealmConfiguration
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class ApiApp : Application() {
    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        val config = RealmConfiguration.Builder()
            .name("myRealm_DB")
            .deleteRealmIfMigrationNeeded()
            .build()
        Realm.setDefaultConfiguration(config)

        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@ApiApp)
            modules(Modules.get())
        }
    }
}