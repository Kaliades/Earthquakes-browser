package com.kulinski_simon.apiapp.ui.request_earthquakes

import android.R
import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.kulinski_simon.apiapp.domain.domain_model.Request
import com.kulinski_simon.apiapp.domain.usecases.CreateQuery
import com.kulinski_simon.apiapp.domain.util.DataHelper

class RequestPresenter(private val createQuery: CreateQuery) :
    RequestContract.Presenter {

    private val dataHelper = DataHelper
    private lateinit var view: RequestContract.View
    override fun setView(view: RequestContract.View) {
        this.view = view
    }

    override fun onStart() {
        view.setStartData(dataHelper.getCurrentDataInString())
        view.setEndData(dataHelper.getCurrentDataInString())
    }

    override fun onSearchBtnClick(context: Context) {
        if (!isNetworkAvailable(context)) {
            view.showErrorMessage("Network connection is not available")
            return
        }
        val requestBuilder = Request.Builder()
        addEndAndStartData(requestBuilder)
        addAlertLevel(requestBuilder)
        addMinMagnitude(requestBuilder)
        val stringRequest = createQuery.execute(requestBuilder.build())
        if (!stringRequest.successful)
            view.showErrorMessage(stringRequest.value)
        else view.onComplete(stringRequest.value)
    }

    override fun onDataStartBtnClick(context: Context) {
        showDataPicker(DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
            view.setStartData("$year-${addZeroIfNecessary(month + 1)}-${addZeroIfNecessary(dayOfMonth)}")
        }, context)
    }

    override fun onDataEndBtnClick(context: Context) {
        showDataPicker(DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
            view.setEndData("$year-${addZeroIfNecessary(month + 1)}-${addZeroIfNecessary(dayOfMonth)}")
        }, context)
    }

    @Suppress("DEPRECATION")
    private fun showDataPicker(listener: DatePickerDialog.OnDateSetListener, context: Context) {
        val dialog = DatePickerDialog(
            context,
            R.style.Theme_Holo_Dialog_MinWidth,
            listener,
            DataHelper.year,
            DataHelper.month - 1,
            DataHelper.day
        )
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }

    private fun addZeroIfNecessary(number: Int) = if (number < 10) "0$number" else number.toString()

    private fun addEndAndStartData(requestBuilder: Request.Builder) {
        requestBuilder.startData(view.getStartData())
        requestBuilder.ednData(view.getEndData())
    }

    private fun addAlertLevel(requestBuilder: Request.Builder) {
        if (view.isAlertLevelChecked()) {
            requestBuilder.alertLevel(view.getAlertLevel())
        }
    }

    private fun addMinMagnitude(requestBuilder: Request.Builder) {
        if (view.isMinMagnitudeChecked()) {
            requestBuilder.minMagnitude(view.getMinMagnitude())
        }
    }

    private fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }

}