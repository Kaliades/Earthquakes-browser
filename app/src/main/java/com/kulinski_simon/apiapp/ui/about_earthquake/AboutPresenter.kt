package com.kulinski_simon.apiapp.ui.about_earthquake

import android.content.Intent
import android.net.Uri
import android.util.Log
import com.kulinski_simon.apiapp.domain.domain_model.Earthquake
import com.kulinski_simon.apiapp.domain.usecases.RealmUseCase
import com.kulinski_simon.apiapp.domain.util.DataHelper
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableSingleObserver

class AboutPresenter(
    private val useCase: RealmUseCase,
    private val earthquake: Earthquake
) : AboutContract.Presenter {

    private lateinit var view: AboutContract.View

    override fun setView(view: AboutContract.View) {
        this.view = view
    }

    override fun start() {
        view.setId(earthquake.id)
        view.setMagnitude(earthquake.mag.toString())
        view.setPlace(earthquake.place)
        view.setDmin(earthquake.dmin)
        view.setStatus(earthquake.status)
        view.setTsunami(earthquake.tsunami.toString())
        view.setType(earthquake.type)
        view.setUrl(earthquake.url)
        view.setAlertColor(earthquake.alert)
        view.setTime(DataHelper.getDataWithHoursAndMinutesByMillisecondsAsString(earthquake.time))
        view.setUpdate(DataHelper.getDataWithHoursAndMinutesByMillisecondsAsString(earthquake.time + earthquake.update))
        view.setLocation(getLocationAsStringMatrix())
        checkIsInDb()
    }

    override fun showOnMapBtn() {
        if (earthquake.coordinates.isEmpty()) {
            view.infoToast("Location is unknown")
            return
        }
        val uri = Uri.parse("geo:${earthquake.coordinates[1]},${earthquake.coordinates[0]}")
        val intent = Intent(Intent.ACTION_VIEW, uri)
        view.startIntent(intent)
    }

    override fun openUrl() {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(earthquake.url))
        view.startIntent(intent)
    }

    override fun saveOrDeleteToDb() {
        if (earthquake.isInDataBase)
            deleteEarthquake()
        else saveEarthquake()
    }

    private fun saveEarthquake() {
        useCase.saveEarthquake(earthquake,
            object : DisposableCompletableObserver() {
                override fun onComplete() {
                    earthquake.isInDataBase = true
                    view.setStartInfo(true)
                }

                override fun onError(e: Throwable) {
                    Log.e(this::class.java.simpleName, e.localizedMessage, e)
                }
            })
    }

    private fun deleteEarthquake() {
        useCase.deleteEarthquake(earthquake.id,
            object : DisposableCompletableObserver() {
                override fun onComplete() {
                    earthquake.isInDataBase = false
                    view.setStartInfo(false)
                }

                override fun onError(e: Throwable) {
                    Log.e(this::class.java.simpleName, e.localizedMessage, e)
                }
            })
    }

    private fun checkIsInDb() {
        useCase.getEarthquak(earthquake.id,
            object : DisposableSingleObserver<Earthquake>() {
                override fun onSuccess(t: Earthquake) {
                    if (t.id == earthquake.id) {
                        view.setStartInfo(true)
                        earthquake.isInDataBase = true
                    }
                }

                override fun onError(e: Throwable) {
                    Log.e(this::class.java.simpleName, e.localizedMessage, e)
                }
            })
    }

    private fun getLocationAsStringMatrix(): String {
        if (earthquake.coordinates.isEmpty())
            return "No information about location"
        return "[ ${earthquake.coordinates[1]}, ${earthquake.coordinates[0]}]"
    }
}