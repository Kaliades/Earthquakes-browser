package com.kulinski_simon.apiapp.ui.list_earthquakes

import com.kulinski_simon.apiapp.domain.domain_model.Earthquake

interface ListContract {

    interface View {
        fun getRequestFromParentActivity(): String
        fun setUpAdapterAndRecyclerView(list: List<Earthquake>, listener: OnItemClickListener)
        fun onError(message: String)
        fun showLoading()
        fun startAboutEarthquake(item: Earthquake)
        fun showToast(message: String)
    }

    interface Presenter {
        fun start()
        fun stop()
        fun setView(view: View)
    }

    interface OnItemClickListener {
        fun onDetailsEarthquake(item: Earthquake)
        fun addToDb(item: Earthquake)
    }
}