package com.kulinski_simon.apiapp.ui.main

import com.kulinski_simon.apiapp.domain.domain_model.Earthquake

interface MainActivityCallBack {
    interface BuildRequest {
        fun onBuildRequestCompleted(request: String)
    }

    interface FragmentListOfEarthquakes {
        fun startFragmentAboutEarthquake(item: Earthquake)
    }

    interface FragmentAboutEarthquake{
        fun startListDataBaseOfEarthquake()
    }

    companion object {
        const val KEY = "KEY"
    }
}