package com.kulinski_simon.apiapp.ui.about_earthquake

import android.content.Intent

interface AboutContract {
    interface View {
        fun setPlace(string: String)
        fun setMagnitude(string: String)
        fun setTime(string: String)
        fun setUpdate(string: String)
        fun setId(string: String)
        fun setType(string: String)
        fun setTsunami(string: String)
        fun setStatus(string: String)
        fun setDmin(string: String)
        fun setUrl(string: String)
        fun setLocation(string: String)
        fun setAlertColor(alert: String)
        fun startIntent(intent: Intent)
        fun infoToast(message: String)
        fun setStartInfo(boolean: Boolean)
    }

    interface Presenter {
        fun setView(view: View)
        fun start()
        fun showOnMapBtn()
        fun openUrl()
        fun saveOrDeleteToDb()
    }

}