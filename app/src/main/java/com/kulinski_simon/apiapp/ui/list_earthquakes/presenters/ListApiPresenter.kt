package com.kulinski_simon.apiapp.ui.list_earthquakes.presenters

import android.util.Log
import com.kulinski_simon.apiapp.domain.domain_model.Earthquake
import com.kulinski_simon.apiapp.domain.domain_model.EarthquakesList
import com.kulinski_simon.apiapp.domain.usecases.GetEarthquakesCollectionsFromApi
import com.kulinski_simon.apiapp.ui.list_earthquakes.ListContract
import io.reactivex.observers.DisposableSingleObserver

class ListApiPresenter(
    private val listUseCase: GetEarthquakesCollectionsFromApi
) : ListContract.Presenter, ListContract.OnItemClickListener {

    private lateinit var view: ListContract.View

    override fun start() {
        val request = view.getRequestFromParentActivity()
        view.showLoading()
        listUseCase.execute(object : DisposableSingleObserver<EarthquakesList<Earthquake>>() {
            override fun onSuccess(t: EarthquakesList<Earthquake>) {
                when {
                    t.hasError -> view.onError(t.errorMessage)
                    else -> view.setUpAdapterAndRecyclerView(t.list, this@ListApiPresenter)
                }
            }

            override fun onError(e: Throwable) {
                Log.e("Error", e.localizedMessage)
            }
        }, request)
    }

    override fun stop() {
        listUseCase.clear()
    }

    override fun setView(view: ListContract.View) {
        this.view = view
    }

    override fun onDetailsEarthquake(item: Earthquake) {
        view.startAboutEarthquake(item)
    }

    override fun addToDb(item: Earthquake) {
    }
}