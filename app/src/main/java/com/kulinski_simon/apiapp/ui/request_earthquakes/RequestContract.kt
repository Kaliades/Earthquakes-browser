package com.kulinski_simon.apiapp.ui.request_earthquakes

import android.content.Context

interface RequestContract {
    interface Presenter {
        fun setView(view: View)
        fun onStart()
        fun onSearchBtnClick(context: Context)
        fun onDataStartBtnClick(context: Context)
        fun onDataEndBtnClick(context: Context)
    }

    interface View {
        fun showErrorMessage(message: String)
        fun onComplete(request: String)
        fun getStartData(): String
        fun getEndData(): String
        fun getMinMagnitude(): String
        fun setStartData(string: String)
        fun setEndData(string: String)
        fun isMinMagnitudeChecked(): Boolean
        fun isAlertLevelChecked(): Boolean
        fun getAlertLevel(): String
    }
}