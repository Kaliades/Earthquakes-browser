package com.kulinski_simon.apiapp.ui.about_earthquake

import android.content.Context
import android.content.Intent
import android.graphics.LightingColorFilter
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.*
import android.widget.Toast
import com.kulinski.szymon.apiapp.R
import com.kulinski_simon.apiapp.ui.main.MainActivityCallBack
import kotlinx.android.synthetic.main.fragment_about_earthquake.*


class AboutFragment : Fragment(), AboutContract.View {

    private lateinit var presenter: AboutContract.Presenter

    private lateinit var callBack: MainActivityCallBack.FragmentAboutEarthquake

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MainActivityCallBack.FragmentAboutEarthquake)
            callBack = context
        else throw ClassCastException(activity.toString() + " must implement  MainActivityCallBack.FragmentAboutEarthquake")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_about_earthquake, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.setView(this)
        presenter.start()
        about_showOnMapBtn.setOnClickListener { presenter.showOnMapBtn() }
        about_url.setOnClickListener { presenter.openUrl() }
        about_star.setOnClickListener { presenter.saveOrDeleteToDb() }
    }
    override fun setPlace(string: String) {
        about_place.text = string
    }

    override fun setMagnitude(string: String) {
        about_magnitude.text = string
    }

    override fun setTime(string: String) {
        about_time.text = string
    }

    override fun setUpdate(string: String) {
        about_update.text = string
    }

    override fun setId(string: String) {
        about_id.text = string
    }

    override fun setType(string: String) {
        about_type.text = string
    }

    override fun setTsunami(string: String) {
        about_tsunami.text = string
    }

    override fun setStatus(string: String) {
        about_status.text = string
    }

    override fun setDmin(string: String) {
        about_dmin.text = string
    }

    override fun setUrl(string: String) {
        about_url.text = string
    }

    override fun setLocation(string: String) {
        about_location.text = string
    }

    override fun setAlertColor(alert: String) {
        when (alert) {
            "green" -> {
                setMagnitudeAlertColor(ContextCompat.getColor(context!!, R.color.alertGreen))
            }
            "yellow" -> {
                setMagnitudeAlertColor(ContextCompat.getColor(context!!, R.color.alertYellow))

            }
            "orange" -> {
                setMagnitudeAlertColor(ContextCompat.getColor(context!!, R.color.alertOrange))

            }
            "red" -> {
                setMagnitudeAlertColor(ContextCompat.getColor(context!!, R.color.alertRed))

            }
        }

    }

    override fun startIntent(intent: Intent) {
        startActivity(intent)
    }

    override fun infoToast(message: String) {
        Toast.makeText(context!!, message, Toast.LENGTH_SHORT).show()
    }

    override fun setStartInfo(boolean: Boolean) {
        if (boolean)
            setStarColor(ContextCompat.getColor(context!!, R.color.gold_star))
        else
            setStarColor(ContextCompat.getColor(context!!, R.color.grey_star))
    }

    private fun setStarColor(color: Int) {
        about_star.setColorFilter(color)

    }

    private fun setMagnitudeAlertColor(color: Int) {
        val circle = about_magnitude.background as Drawable
        circle.colorFilter = LightingColorFilter(color, color)
    }

    private fun setPresenter(presenter: AboutContract.Presenter): Fragment {
        this.presenter = presenter
        return this
    }

    companion object {
        fun newInstance(presenter: AboutContract.Presenter)=
            AboutFragment().setPresenter(presenter)
    }
}