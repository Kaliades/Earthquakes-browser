package com.kulinski_simon.apiapp.ui.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.kulinski.szymon.apiapp.R
import com.kulinski.szymon.apiapp.R.id
import com.kulinski.szymon.apiapp.R.layout
import com.kulinski_simon.apiapp.domain.domain_model.Earthquake
import com.kulinski_simon.apiapp.domain.usecases.RealmUseCase
import com.kulinski_simon.apiapp.ui.about_earthquake.AboutFragment
import com.kulinski_simon.apiapp.ui.about_earthquake.AboutPresenter
import com.kulinski_simon.apiapp.ui.list_earthquakes.ListContract
import com.kulinski_simon.apiapp.ui.list_earthquakes.ListFragment
import com.kulinski_simon.apiapp.ui.request_earthquakes.RequestFragment
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.core.qualifier.named

class MainActivity : AppCompatActivity(), MainActivityCallBack.BuildRequest,
    MainActivityCallBack.FragmentListOfEarthquakes,
    MainActivityCallBack.FragmentAboutEarthquake {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_main)
        setSupportActionBar(toolbar)
        supportFragmentManager.beginTransaction()
            .add(
                R.id.activity_main_fragmentContainer,
                RequestFragment.getNewInstance()
            ).commit()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            id.main_show_db_list -> {
                startListDataBaseOfEarthquake()
                return true

            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBuildRequestCompleted(request: String) {
        val bundle = Bundle()
        bundle.putString(MainActivityCallBack.KEY, request)
        val presenter: ListContract.Presenter by inject(named("Api"))
        val fragment = ListFragment.getInstance(presenter)
        fragment.arguments = bundle
        supportFragmentManager.beginTransaction().replace(
            R.id.activity_main_fragmentContainer,
            fragment
        ).addToBackStack(null).commit()
    }

    override fun startFragmentAboutEarthquake(item: Earthquake) {
        supportFragmentManager.beginTransaction().replace(
            R.id.activity_main_fragmentContainer,
            AboutFragment.newInstance(
                AboutPresenter(
                    inject<RealmUseCase>().value,
                    item
                )
            )
        ).addToBackStack(null).commit()
    }

    override fun startListDataBaseOfEarthquake() {
        val presenter: ListContract.Presenter by inject(named("Realm"))
        val fragment = ListFragment.getInstance(presenter)
        supportFragmentManager.beginTransaction().replace(
            R.id.activity_main_fragmentContainer,
            fragment
        ).addToBackStack(null).commit()
    }

}
