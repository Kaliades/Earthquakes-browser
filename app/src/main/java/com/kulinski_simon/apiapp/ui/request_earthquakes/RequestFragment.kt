package com.kulinski_simon.apiapp.ui.request_earthquakes

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.kulinski.szymon.apiapp.R
import com.kulinski_simon.apiapp.ui.main.MainActivityCallBack
import kotlinx.android.synthetic.main.fragment_request_earthquakes.*
import org.koin.android.ext.android.inject

class RequestFragment : Fragment(), RequestContract.View {

    private val presenter: RequestContract.Presenter by inject()
    private lateinit var mCallBack: MainActivityCallBack.BuildRequest


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MainActivityCallBack.BuildRequest)
            mCallBack = context
        else {
            throw ClassCastException(activity.toString() + " must implement MainActivityCallBack")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        presenter.setView(this)
        return inflater.inflate(R.layout.fragment_request_earthquakes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onStart()
        searchBtn.setOnClickListener { presenter.onSearchBtnClick(context!!) }
        startDataCalendar.setOnClickListener { presenter.onDataStartBtnClick(context!!) }
        endDataCalendar.setOnClickListener { presenter.onDataEndBtnClick(context!!) }
        checkBox_AlertLevel.setOnClickListener {
            if (!checkBox_AlertLevel.isChecked) {
                checkBox_AlertLevel.isChecked = false
                radioGroup.visibility = View.GONE
                radioGroup.clearCheck()
            } else {
                checkBox_AlertLevel.isChecked = true
                radioGroup.visibility = View.VISIBLE
            }
        }
        checkBox_MinMagnitude.setOnClickListener {
            if (!checkBox_MinMagnitude.isChecked) {
                checkBox_MinMagnitude.isChecked = false
                magEditText.visibility = View.GONE
                magEditText.setText("")
            } else {
                checkBox_MinMagnitude.isChecked = true
                magEditText.visibility = View.VISIBLE
            }
        }
    }


    //------------------------------------------------------------------------------------------------------------------

    override fun showErrorMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override fun onComplete(request: String) {
        mCallBack.onBuildRequestCompleted(request)
    }

    override fun getStartData(): String {
        return startDataEditText.text.toString()
    }

    override fun getEndData(): String {
        return endDataEditText.text.toString()
    }

    override fun getMinMagnitude(): String {
        return magEditText.text.toString()
    }

    override fun setStartData(string: String) {
        startDataEditText.setText(string)
    }

    override fun setEndData(string: String) {
        endDataEditText.setText(string)
    }

    override fun isMinMagnitudeChecked(): Boolean {
        return checkBox_MinMagnitude.isChecked
    }

    override fun isAlertLevelChecked(): Boolean {
        return checkBox_AlertLevel.isChecked
    }

    override fun getAlertLevel(): String {
        return when (radioGroup.checkedRadioButtonId) {
            greenRB.id -> "green"
            yellowRB.id -> "yellow"
            orangeRB.id -> "orange"
            redRB.id -> "red"
            else -> ""
        }
    }
    //------------------------------------------------------------------------------------------------------------------

    companion object {
        fun getNewInstance(): Fragment {
            return RequestFragment()
        }
    }
}