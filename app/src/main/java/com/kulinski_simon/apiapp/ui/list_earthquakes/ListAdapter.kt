package com.kulinski_simon.apiapp.ui.list_earthquakes

import android.content.Context
import android.graphics.LightingColorFilter
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.kulinski.szymon.apiapp.R
import com.kulinski_simon.apiapp.domain.domain_model.Earthquake
import com.kulinski_simon.apiapp.domain.util.DataHelper
import kotlinx.android.synthetic.main.list_item_earthquakes.view.*

class ListAdapter(
    private val listOfItems: List<Earthquake>,
    private val listener: ListContract.OnItemClickListener,
    private val context: Context
) :
    RecyclerView.Adapter<ListAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val place = view.item_place!!
        val magnitude = view.item_magnitude!!
        val time = view.item_time!!
        val layout = view.list_earthquakes_layout!!
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(p0.context).inflate(
                R.layout.list_item_earthquakes,
                p0,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return listOfItems.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.place.text = listOfItems[position].place
        viewHolder.magnitude.text = listOfItems[position].mag.toString()
        viewHolder.time.text = DataHelper.getDataByMillisecondsAsString(listOfItems[position].time)
        setBackgroundForMagnitude(listOfItems[position].alert, viewHolder.magnitude)
        viewHolder.layout.setOnClickListener { listener.onDetailsEarthquake(listOfItems[position]) }
    }

    private fun setBackgroundForMagnitude(alert: String, magnitude: TextView) {
        when (alert) {
            "green" -> {
                val color = ContextCompat.getColor(context, R.color.alertGreen)
                setColor(color, magnitude)
            }
            "yellow" -> {
                val color = ContextCompat.getColor(context, R.color.alertYellow)
                setColor(color, magnitude)
            }
            "orange" -> {
                val color = ContextCompat.getColor(context, R.color.alertOrange)
                setColor(color, magnitude)
            }
            "red" -> {
                val color = ContextCompat.getColor(context, R.color.alertRed)
                setColor(color, magnitude)
            }
        }
    }

    private fun setColor(color: Int, magnitude: TextView) {
        val circle = magnitude.background as Drawable
        circle.colorFilter = LightingColorFilter(color, color)
    }
}
