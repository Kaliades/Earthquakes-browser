package com.kulinski_simon.apiapp.ui.list_earthquakes

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.kulinski_simon.apiapp.ui.main.MainActivity
import com.kulinski.szymon.apiapp.R
import com.kulinski_simon.apiapp.domain.domain_model.Earthquake
import com.kulinski_simon.apiapp.ui.main.MainActivityCallBack
import kotlinx.android.synthetic.main.fragment_list_earthquakes.*

class ListFragment : Fragment(), ListContract.View {

    private lateinit var presenter: ListContract.Presenter

    lateinit var callBack: MainActivityCallBack.FragmentListOfEarthquakes

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MainActivityCallBack.FragmentListOfEarthquakes)
            callBack = context
        else throw ClassCastException(activity.toString() + " must implement  MainActivityCallBack.FragmentListOfEarthquakes")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list_earthquakes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.setView(this)
        presenter.start()
    }

    override fun getRequestFromParentActivity(): String {
        return arguments?.getString(MainActivityCallBack.KEY) ?: ""
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.stop()
    }

    override fun setUpAdapterAndRecyclerView(
        list: List<Earthquake>,
        listener: ListContract.OnItemClickListener
    ) {
        fragment_list_earthquakes_progressBarr.hide()
        fragment_list_earthquakes_recyclerView.visibility = View.VISIBLE
        fragment_list_earthquakes_recyclerView.layoutManager = LinearLayoutManager(context)
        fragment_list_earthquakes_recyclerView.adapter =
            ListAdapter(list, listener, context!!)
        Toast.makeText(context, "List size:" + list.size, Toast.LENGTH_SHORT).show()
    }

    override fun onError(message: String) {
        fragment_list_earthquakes_progressBarr.hide()
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        startActivity(Intent(context, MainActivity::class.java))
    }

    override fun showLoading() {
        fragment_list_earthquakes_progressBarr.show()
        fragment_list_earthquakes_recyclerView.visibility = View.INVISIBLE
    }

    override fun startAboutEarthquake(item: Earthquake) {
        callBack.startFragmentAboutEarthquake(item)
    }

    override fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    private fun setPresenter(presenter: ListContract.Presenter): Fragment {
        this.presenter = presenter
        return this
    }

    companion object {
        fun getInstance(presenter: ListContract.Presenter): Fragment {
            return ListFragment().setPresenter(presenter)
        }
    }
}