package com.kulinski_simon.apiapp.data

import com.kulinski_simon.apiapp.data.api.EarthquakeApiAdapter
import com.kulinski_simon.apiapp.data.converters.ConverterFeatureCollectionToEarthquakesCollection
import com.kulinski_simon.apiapp.data.realm_db.RealmDao
import com.kulinski_simon.apiapp.domain.domain_model.Earthquake
import com.kulinski_simon.apiapp.domain.repository.EarthquakeRepository
import io.reactivex.Completable
import io.reactivex.Single

class EarthquakeRepositoryImpl(
    private val realmDao: RealmDao,
    private val apiAdapter: EarthquakeApiAdapter
) : EarthquakeRepository<Earthquake> {

    private val apiConverter =
        ConverterFeatureCollectionToEarthquakesCollection

    override fun saveEarthquake(item: Earthquake): Completable {
        return realmDao.saveEarthquake(item)
    }

    override fun getEarthquakeById(id: String): Single<Earthquake> {
        return realmDao.getEarthquake(id)
    }

    override fun getListOfAllEarthquakeFromDb(): Single<List<Earthquake>> {
        return realmDao.getListOfAllEarthquakes()
    }

    override fun getListOfEarthquakeFromApi(request: String): Single<List<Earthquake>> {
        return apiAdapter.getEarthquakes(request).map { apiConverter.convert(it) }
    }

    override fun deleteEarthquake(id: String): Completable {
        return realmDao.deleteEarthquake(id)
    }
}