package com.kulinski_simon.apiapp.data.model.api

data class FeatureCollection(val features: List<Feature>) {

    data class Feature(
        val id: String?,
        val properties: Properties,
        val geometry: Geometry
    )

    data class Properties(
        val mag: Double?,
        val place: String?,
        val time: Long?,
        val update: Long?,
        val alert: String?,
        val type: String?,
        val tsunami: Int?,
        val title: String?,
        val status: String?,
        val url: String?,
        val dmin: String?
    )

    data class Geometry(
        val coordinates: List<Double>?
    )
}