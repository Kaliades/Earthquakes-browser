package com.kulinski_simon.apiapp.data.converters

import com.kulinski_simon.apiapp.data.model.realm.RealmEarthquake
import com.kulinski_simon.apiapp.domain.converters.ConverterDbModelDomainModel
import com.kulinski_simon.apiapp.domain.domain_model.Earthquake
import io.realm.RealmList

object ConverterRealmModelDomainModel :
    ConverterDbModelDomainModel<RealmEarthquake, Earthquake> {

    override fun dbModelToDomainModel(dbModel: RealmEarthquake): Earthquake {
        return Earthquake(
            dbModel.id ?: "-1",
            dbModel.mag ?: 0.0,
            dbModel.place ?: "no information",
            dbModel.time ?: 0,
            dbModel.update ?: 0,
            dbModel.alert ?: "no information",
            dbModel.type ?: "no information",
            dbModel.tsunami ?: 0,
            dbModel.title ?: "no information",
            dbModel.status ?: "no information",
            dbModel.url ?: "",
            dbModel.dmin ?: "no information",
            dbModel.coordinates?.toList() ?: emptyList()
        )


    }

    override fun domainModelToDbModel(domainModel: Earthquake): RealmEarthquake {
        val realmItem = RealmEarthquake()
        realmItem.id = domainModel.id
        realmItem.mag = domainModel.mag
        realmItem.alert = domainModel.alert
        realmItem.place = domainModel.place
        realmItem.time = domainModel.time
        realmItem.title = domainModel.title
        realmItem.tsunami = domainModel.tsunami
        realmItem.type = domainModel.type
        realmItem.update = domainModel.update
        realmItem.status = domainModel.status
        realmItem.url = domainModel.url
        realmItem.dmin = domainModel.url
        realmItem.coordinates = RealmList()
        domainModel.coordinates.forEach { realmItem.coordinates!!.add(it) }
        return realmItem
    }

    override fun listOfDomainModelsToListOfDbModels(listOfDomainModels: List<Earthquake>): List<RealmEarthquake> {
        return listOfDomainModels.map {
            domainModelToDbModel(
                it
            )
        }
    }

    override fun lifOfDbModelsToListOfDomainModels(listOfDbModels: List<RealmEarthquake>): List<Earthquake> {
        return listOfDbModels.map {
            dbModelToDomainModel(
                it
            )
        }
    }


}
