package com.kulinski_simon.apiapp.data.api

import com.kulinski_simon.apiapp.data.model.api.FeatureCollection
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Url

object EarthquakeApiAdapter {

    private val interceptor = HttpLoggingInterceptor()
    private val okHttpClient = OkHttpClient.Builder()
        .retryOnConnectionFailure(true)
        .addInterceptor(interceptor)
        .build()
    private val retrofit = Retrofit.Builder()
        .baseUrl(UrlManager.API_HOST)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    init {
        interceptor.level = HttpLoggingInterceptor.Level.BASIC
    }

    private val service = retrofit.create(EarthquakeApiAdapter.EarthquakeService::class.java)


    fun getEarthquakes(request: String): Single<FeatureCollection> {
        return service.getEarthQuakes(request)
    }

    interface EarthquakeService {
        @GET
        fun getEarthQuakes(@Url request: String): Single<FeatureCollection>
    }

}
