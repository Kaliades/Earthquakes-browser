package com.kulinski_simon.apiapp.data.realm_db

import com.kulinski_simon.apiapp.data.model.realm.RealmEarthquake
import com.kulinski_simon.apiapp.data.converters.ConverterRealmModelDomainModel
import com.kulinski_simon.apiapp.domain.domain_model.Earthquake
import io.reactivex.Completable
import io.reactivex.Single
import io.realm.Realm

object RealmDao {

    private val converter = ConverterRealmModelDomainModel

    fun saveEarthquake(dbModel: Earthquake): Completable {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm -> realm.copyToRealmOrUpdate(converter.domainModelToDbModel(dbModel)) }
        }
        return Completable.complete()
    }

    fun saveListOfEarthquake(listOfDbModels: List<Earthquake>): Completable {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                realm.copyToRealmOrUpdate(
                    converter.listOfDomainModelsToListOfDbModels(
                        listOfDbModels
                    )
                )
            }
        }
        return Completable.complete()
    }

    fun getEarthquake(id: String): Single<Earthquake> {
        return Realm.getDefaultInstance().use {
            Single.just(
                converter.dbModelToDomainModel(
                    RealmEarthquake.prepareModelToSend(
                        it.where(RealmEarthquake::class.java)
                            .equalTo("id", id)
                            .findFirst()
                    )
                )
            )
        }
    }

    fun getListOfAllEarthquakes(): Single<List<Earthquake>> {
        return Realm.getDefaultInstance().use {
            Single.just(it.where(RealmEarthquake::class.java).findAll().toList().map { item ->
                converter.dbModelToDomainModel(item)
            })
        }
    }

    fun deleteEarthquake(id: String): Completable {
        Realm.getDefaultInstance().use { delete(id, it) }
        return Completable.complete()
    }

    fun deleteListOfEarthquake(listOfIds: List<String>): Completable {
        Realm.getDefaultInstance().use {
            listOfIds.forEach { id -> delete(id, it) }
        }
        return Completable.complete()
    }

    fun deleteAllOfEarthquake(): Completable {
        Realm.getDefaultInstance()
            .executeTransaction { it.where(RealmEarthquake::class.java).findAll().deleteAllFromRealm() }
        return Completable.complete()
    }

    private fun delete(id: String, realm: Realm) {
        realm.executeTransaction {
            it.where(RealmEarthquake::class.java)
                .equalTo("id", id)
                .findFirst()
                ?.deleteFromRealm()
        }
    }

}