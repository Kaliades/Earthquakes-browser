package com.kulinski_simon.apiapp.data.model.realm

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey

open class RealmEarthquake : RealmObject() {
    @PrimaryKey
    open var id: String? = null
    open var mag: Double? = null
    open var place: String? = null
    open var time: Long? = null
    open var update: Long? = null
    open var alert: String? = null
    open var type: String? = null
    open var tsunami: Int? = null
    open var title: String? = null
    open var status: String? = null
    open var url: String? = null
    open var dmin: String? = null
    open var coordinates: RealmList<Double>? = null
    @Ignore
    var isFound: Boolean? = null
    @Ignore
    var message: String? = null

    companion object {
        fun prepareModelToSend(realmModel: RealmEarthquake?): RealmEarthquake {
            if (realmModel == null)
                return createNotFoundModel()
            realmModel.isFound = true
            return realmModel
        }

        private fun createNotFoundModel(): RealmEarthquake {
            val realmModel = RealmEarthquake()
            realmModel.isFound = false
            realmModel.message = "Earthquake not found !"
            return realmModel
        }
    }
}