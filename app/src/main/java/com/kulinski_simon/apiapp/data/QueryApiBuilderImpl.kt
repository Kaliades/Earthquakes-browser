package com.kulinski_simon.apiapp.data

import com.kulinski_simon.apiapp.data.api.UrlManager
import com.kulinski_simon.apiapp.domain.domain_model.Request
import com.kulinski_simon.apiapp.domain.repository.QueryApiBuilder
import java.lang.StringBuilder

class QueryApiBuilderImpl : QueryApiBuilder {
    override fun build(request: Request): String {
        val stringBuilder = StringBuilder(UrlManager.QUERY)
        stringBuilder.append("&${UrlManager.QUERY_START_DATA}${request.startData}")
        stringBuilder.append("&${UrlManager.QUERY_END_DATA}${request.endData}")
        if (!request.minMagnitude.isEmpty())
            stringBuilder.append("&${UrlManager.QUERY_MIN_MAGNITUDE}${request.minMagnitude}")
        if (!request.alertLevel.isEmpty())
            stringBuilder.append("&${UrlManager.QUERY_ALERT_LEVEL}${request.alertLevel}")
        return stringBuilder.toString()
    }

}