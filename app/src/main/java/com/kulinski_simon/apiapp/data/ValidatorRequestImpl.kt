package com.kulinski_simon.apiapp.data

import com.kulinski_simon.apiapp.domain.domain_model.Request
import com.kulinski_simon.apiapp.domain.repository.ValidatorRequest

class ValidatorRequestImpl : ValidatorRequest {

    override fun checkValidity(request: Request): ValidatorRequest.Result {
        if (!checkData(request.startData))
            return ValidatorRequest.Result.invalidRequest("Start data is invalid")
        if (!checkData(request.endData))
            return ValidatorRequest.Result.invalidRequest("End data is invalid")
        if (!checkEndDataIsGreaterThanStartData(request.startData, request.endData))
            return ValidatorRequest.Result.invalidRequest("End data mus be greater then start data")
        if (!checkMinMagnitude(request.minMagnitude))
            return ValidatorRequest.Result.invalidRequest("Min magnitude is invalid")
        if (!checkAlertLevel(request.alertLevel))
            return ValidatorRequest.Result.invalidRequest("Alert level is invalid")
        return ValidatorRequest.Result.validRequest()

    }

    private fun checkData(data: String) = "^[1|2]\\d{3}-\\d{2}-\\d{2}".toRegex().matches(data)

    private fun checkEndDataIsGreaterThanStartData(start: String, end: String): Boolean {
        val startDay = start.substring(8, 10).toDouble()
        val endDay = end.substring(8, 10).toDouble()
        val startMonth = start.substring(6, 7).toDouble()
        val endMonth = end.substring(6, 7).toDouble()
        val startYear = start.substring(0, 4)
        val endYear = end.substring(0, 4)
        if (endYear > startYear)
            return true
        if (endMonth > startMonth && endYear == startYear)
            return true
        if (endMonth == startMonth && endYear == startYear)
            if (endDay > startDay)
                return true
        return false
    }

    private fun checkMinMagnitude(minMagnitude: String): Boolean {
        if (!minMagnitude.isEmpty())
            return "^\\d+(\\.\\d)?".toRegex().matches(minMagnitude)
        return true
    }

    private fun checkAlertLevel(alertLevel: String): Boolean {
        if (!alertLevel.isEmpty())
            return "\\D+".toRegex().matches(alertLevel)
        return true
    }


}