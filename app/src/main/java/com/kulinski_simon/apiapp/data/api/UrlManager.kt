package com.kulinski_simon.apiapp.data.api

object UrlManager {
    const val API_HOST = "https://earthquake.usgs.gov/fdsnws/event/1/"
    const val QUERY = "query?format=geojson"
    const val QUERY_START_DATA = "starttime="
    const val QUERY_END_DATA = "endtime="
    const val QUERY_MIN_MAGNITUDE = "minmagnitude="
    const val QUERY_ALERT_LEVEL = "alertlevel="
}