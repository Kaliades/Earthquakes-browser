package com.kulinski_simon.apiapp.data.converters

import com.kulinski_simon.apiapp.data.model.api.FeatureCollection
import com.kulinski_simon.apiapp.domain.converters.ConverterApiModelDomainModel
import com.kulinski_simon.apiapp.domain.domain_model.Earthquake

object ConverterFeatureCollectionToEarthquakesCollection :
    ConverterApiModelDomainModel<FeatureCollection, List<Earthquake>> {
    override fun convert(apiModel: FeatureCollection): List<Earthquake> {
        return apiModel.features.map {
            convertToViewModelEarthquake(
                it
            )
        }
    }

    private fun convertToViewModelEarthquake(features: FeatureCollection.Feature): Earthquake {
        val properties = features.properties
        return Earthquake(
            features.id ?: "non value",
            properties.mag ?: 0.0,
            properties.place ?: "non value",
            properties.time ?: 0,
            properties.update ?: 0,
            properties.alert ?: "non value",
            properties.type ?: "non value",
            properties.tsunami ?: 0,
            properties.title ?: "non value",
            properties.status ?: "non value",
            properties.url ?: "non value",
            properties.dmin ?: "non value",
            features.geometry.coordinates ?: emptyList()
        )
    }

}