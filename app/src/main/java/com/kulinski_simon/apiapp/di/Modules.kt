package com.kulinski_simon.apiapp.di

import com.kulinski_simon.apiapp.data.QueryApiBuilderImpl
import com.kulinski_simon.apiapp.data.EarthquakeRepositoryImpl
import com.kulinski_simon.apiapp.data.ValidatorRequestImpl
import com.kulinski_simon.apiapp.data.api.EarthquakeApiAdapter
import com.kulinski_simon.apiapp.data.realm_db.RealmDao
import com.kulinski_simon.apiapp.domain.domain_model.Earthquake
import com.kulinski_simon.apiapp.domain.repository.QueryApiBuilder
import com.kulinski_simon.apiapp.domain.repository.EarthquakeRepository
import com.kulinski_simon.apiapp.domain.repository.ValidatorRequest
import com.kulinski_simon.apiapp.domain.usecases.CreateQuery
import com.kulinski_simon.apiapp.domain.usecases.GetEarthquakesCollectionsFromApi
import com.kulinski_simon.apiapp.domain.usecases.GetEarthquakesCollectionsFromRealm
import com.kulinski_simon.apiapp.domain.usecases.RealmUseCase
import com.kulinski_simon.apiapp.domain.util.SchedulerProviderImpl
import com.kulinski_simon.apiapp.ui.list_earthquakes.ListContract
import com.kulinski_simon.apiapp.ui.list_earthquakes.presenters.ListApiPresenter
import com.kulinski_simon.apiapp.ui.list_earthquakes.presenters.ListRealmPresenter
import com.kulinski_simon.apiapp.ui.request_earthquakes.RequestContract
import com.kulinski_simon.apiapp.ui.request_earthquakes.RequestPresenter
import org.koin.core.qualifier.named
import org.koin.dsl.module

object Modules {

    fun get() = listOf(
        dataModule,
        requestModule,
        listApiModule,
        listDbModule,
        aboutModule
    )

    private val dataModule = module {
        single { EarthquakeRepositoryImpl(
            RealmDao,
            EarthquakeApiAdapter
        ) as EarthquakeRepository<Earthquake>
        }
    }
    private val requestModule = module {
        single { ValidatorRequestImpl() as ValidatorRequest }
        single { QueryApiBuilderImpl() as QueryApiBuilder }
        single { CreateQuery(get(), get()) }
        single { RequestPresenter(get()) as RequestContract.Presenter }
    }

    private val listApiModule = module {
        single {
            GetEarthquakesCollectionsFromApi(
                get(),
                SchedulerProviderImpl
            )
        }
        single(named("Api")) { ListApiPresenter(get()) as ListContract.Presenter }
    }

    private val listDbModule = module {
        single {
            GetEarthquakesCollectionsFromRealm(
                get(),
                SchedulerProviderImpl
            )
        }
        single(named("Realm")) { ListRealmPresenter(get()) as ListContract.Presenter }
    }

    private val aboutModule = module {
        single {
            RealmUseCase(
                get(),
                SchedulerProviderImpl
            )
        }
        // single{AboutPresenter(get()) as }
    }
}