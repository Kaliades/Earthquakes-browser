package com.kulinski_simon.apiapp.domain.util

import java.text.SimpleDateFormat
import java.util.*

object DataHelper {

    private val calendar = Calendar.getInstance()
    val year = calendar.get(Calendar.YEAR)
    val month = calendar.get(Calendar.MONTH) + 1
    val day = calendar.get(Calendar.DAY_OF_MONTH)

    fun getCurrentDataInString(): String {
        return when {
            (month < 10 && day < 10) -> "$year-0$month-0$day"
            (month < 10 && day > 10) -> "$year-0$month-$day"
            (month > 10 && day < 10) -> "$year-$month-0$day"
            else -> "$year-$month-$day"
        }
    }

    fun getDataByMillisecondsAsString(longTime: Long) =
        SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(Date(longTime))!!

    fun getDataWithHoursAndMinutesByMillisecondsAsString(longTime: Long) =
        SimpleDateFormat("yyyy-MM-dd HH:mm a", Locale.getDefault()).format(Date(longTime))!!


}

