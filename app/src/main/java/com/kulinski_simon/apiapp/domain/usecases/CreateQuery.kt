package com.kulinski_simon.apiapp.domain.usecases

import com.kulinski_simon.apiapp.domain.SynchronousUseCase
import com.kulinski_simon.apiapp.domain.domain_model.Request
import com.kulinski_simon.apiapp.domain.repository.QueryApiBuilder
import com.kulinski_simon.apiapp.domain.repository.ValidatorRequest

class CreateQuery(private val validator: ValidatorRequest, private val queryApiBuilder: QueryApiBuilder) :
    SynchronousUseCase<Request.StringRequest, Request> {
    override fun execute(params: Request): Request.StringRequest {
        val isValidatorRequest = validator.checkValidity(params)
        if (!isValidatorRequest.isValid)
            return Request.StringRequest.createFailureRequest(isValidatorRequest.error)
        val request = queryApiBuilder.build(params)
        return Request.StringRequest.createSuccessRequest(request)
    }
}

