package com.kulinski_simon.apiapp.domain

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseReactiveUseCase {

    private val compositeDisposable = CompositeDisposable()

    open fun clear() {
            compositeDisposable.clear()
    }

    protected fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }
}