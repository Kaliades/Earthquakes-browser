package com.kulinski_simon.apiapp.domain

import com.kulinski_simon.apiapp.domain.util.SchedulerProvider
import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver

abstract class SingleUseCase<Result, Params>(private val schedulerProvider: SchedulerProvider) :
    BaseReactiveUseCase() {

    abstract fun build(params: Params): Single<Result>

    fun execute(observer: DisposableSingleObserver<Result>, params: Params) {
        val single = buildSingleUseCaseWithSchedulers(params)
        addDisposable(single.subscribeWith(observer))
    }


    private fun buildSingleUseCaseWithSchedulers(params: Params): Single<Result> {
        return build(params)
            .subscribeOn(schedulerProvider.getIoScheduler())
            .observeOn(schedulerProvider.getUiScheduler())
    }


}