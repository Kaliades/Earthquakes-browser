package com.kulinski_simon.apiapp.domain.converters

interface ConverterDbModelDomainModel<DbModel, DomainModel> {

    fun dbModelToDomainModel(dbModel: DbModel): DomainModel

    fun domainModelToDbModel(domainModel: DomainModel): DbModel

    fun listOfDomainModelsToListOfDbModels(listOfDomainModels: List<DomainModel>): List<DbModel>

    fun lifOfDbModelsToListOfDomainModels(listOfDbModels: List<DbModel>): List<DomainModel>
}