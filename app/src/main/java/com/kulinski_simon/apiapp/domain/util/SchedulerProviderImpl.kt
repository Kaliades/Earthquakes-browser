package com.kulinski_simon.apiapp.domain.util

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object SchedulerProviderImpl : SchedulerProvider {
    override fun getIoScheduler(): Scheduler {
        return Schedulers.io()
    }

    override fun getUiScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}