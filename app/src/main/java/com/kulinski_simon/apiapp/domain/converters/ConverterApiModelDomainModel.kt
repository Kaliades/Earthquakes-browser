package com.kulinski_simon.apiapp.domain.converters

interface ConverterApiModelDomainModel<ApiModel, UiModel> {

    fun convert(apiModel: ApiModel): UiModel

}