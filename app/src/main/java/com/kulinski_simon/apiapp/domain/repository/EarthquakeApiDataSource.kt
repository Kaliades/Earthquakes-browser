package com.kulinski_simon.apiapp.domain.repository

import io.reactivex.Single

interface EarthquakeApiDataSource<ApiModel> {

    fun getListOfEarthquakes(request: String): Single<ApiModel>
}