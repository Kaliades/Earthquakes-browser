package com.kulinski_simon.apiapp.domain.domain_model

data class EarthquakesList<Item>(
    val list: List<Item>,
    val errorMessage: String,
    val hasError: Boolean
)



