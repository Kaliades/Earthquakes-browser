package com.kulinski_simon.apiapp.domain.usecases

import com.kulinski_simon.apiapp.domain.SingleUseCase
import com.kulinski_simon.apiapp.domain.domain_model.Earthquake
import com.kulinski_simon.apiapp.domain.domain_model.EarthquakesList
import com.kulinski_simon.apiapp.domain.repository.EarthquakeRepository
import com.kulinski_simon.apiapp.domain.util.SchedulerProvider
import io.reactivex.Single

class GetEarthquakesCollectionsFromApi(
    private val repository: EarthquakeRepository<Earthquake>,
    schedulerProvider: SchedulerProvider
) : SingleUseCase<EarthquakesList<Earthquake>, String>(schedulerProvider) {
    override fun build(params: String): Single<EarthquakesList<Earthquake>> {
        return repository.getListOfEarthquakeFromApi(params).map {
            if (it.isEmpty())
                EarthquakesList(emptyList(), "Empty list", true)
            else
                EarthquakesList(it, "", false)
        }
    }
}