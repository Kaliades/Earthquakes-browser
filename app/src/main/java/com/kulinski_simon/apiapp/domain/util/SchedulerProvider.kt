package com.kulinski_simon.apiapp.domain.util

import io.reactivex.Scheduler

interface SchedulerProvider {
    fun getIoScheduler(): Scheduler
    fun getUiScheduler(): Scheduler
}