package com.kulinski_simon.apiapp.domain.domain_model

data class Earthquake(
    val id: String,
    val mag: Double,
    val place: String,
    val time: Long,
    val update: Long,
    val alert: String,
    val type: String,
    val tsunami: Int,
    val title: String,
    val status: String,
    val url: String,
    val dmin: String,
    val coordinates: List<Double>
) {
    var isInDataBase = false
}