package com.kulinski_simon.apiapp.domain.domain_model

class ListOfEarthquakes private constructor(
    val listOfEarthquakes: List<Earthquake>,
    val errorMessage: String,
    val hasError: Boolean
) {

    companion object Factory {
        fun error(error: String): ListOfEarthquakes {
            return ListOfEarthquakes(emptyList(), error, hasError = true)
        }

        fun success(list: List<Earthquake>): ListOfEarthquakes {
            return ListOfEarthquakes(list, "", hasError = false)
        }
    }
}