package com.kulinski_simon.apiapp.domain.error

import java.io.IOException

class EmptyDataSetException: IOException() {
    private val errorMessage = "Wrong data, list is empty!"

    override val message: String?
        get() = errorMessage
}