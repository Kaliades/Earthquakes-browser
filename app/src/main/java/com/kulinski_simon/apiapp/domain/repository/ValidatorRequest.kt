package com.kulinski_simon.apiapp.domain.repository

import com.kulinski_simon.apiapp.domain.domain_model.Request

interface ValidatorRequest {

    fun checkValidity(request: Request): Result

    class Result private constructor(val isValid: Boolean, val error: String) {
        companion object {
            fun validRequest(): Result {
                return Result(true, "")
            }

            fun invalidRequest(error: String): Result {
                return Result(false, error)
            }
        }
    }
}