package com.kulinski_simon.apiapp.domain.repository

import com.kulinski_simon.apiapp.domain.domain_model.Request

interface QueryApiBuilder {
    fun build(request: Request): String


}