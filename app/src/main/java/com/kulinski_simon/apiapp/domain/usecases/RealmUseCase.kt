package com.kulinski_simon.apiapp.domain.usecases

import com.kulinski_simon.apiapp.domain.BaseReactiveUseCase
import com.kulinski_simon.apiapp.domain.domain_model.Earthquake
import com.kulinski_simon.apiapp.domain.repository.EarthquakeRepository
import com.kulinski_simon.apiapp.domain.util.SchedulerProvider
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableSingleObserver

class RealmUseCase(
    private val repository: EarthquakeRepository<Earthquake>,
    private val schedulerProvider: SchedulerProvider
) : BaseReactiveUseCase() {

    fun saveEarthquake(earthquake: Earthquake, observer: DisposableCompletableObserver) {
        val completable = buildCompletable(repository.saveEarthquake(earthquake))
        addDisposable(completable.subscribeWith(observer))
    }

    fun deleteEarthquake(id: String, observer: DisposableCompletableObserver) {
        val completable = buildCompletable(repository.deleteEarthquake(id))
        addDisposable(completable.subscribeWith(observer))
    }

    fun getEarthquak(id: String, observer: DisposableSingleObserver<Earthquake>) {
        val single = buildSingle(repository.getEarthquakeById(id))
        addDisposable(single.subscribeWith(observer))
    }

    private fun buildSingle(single: Single<Earthquake>): Single<Earthquake> {
        return single
            .subscribeOn(schedulerProvider.getIoScheduler())
            .observeOn(schedulerProvider.getUiScheduler())
    }

    private fun buildCompletable(completable: Completable): Completable {
        return completable
            .subscribeOn(schedulerProvider.getIoScheduler())
            .observeOn(schedulerProvider.getUiScheduler())
    }

}