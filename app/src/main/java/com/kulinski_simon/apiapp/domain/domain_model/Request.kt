package com.kulinski_simon.apiapp.domain.domain_model


class Request private constructor() {
    var startData = ""
    var endData = ""
    var minMagnitude = ""
    var alertLevel = ""

    class Builder {
        private val preparatory = Request()
        fun startData(startData: String): Builder {
            preparatory.startData = startData
            return this
        }

        fun ednData(endData: String): Builder {
            preparatory.endData = endData
            return this
        }

        fun minMagnitude(minMagnitude: String): Builder {
            preparatory.minMagnitude = minMagnitude
            return this
        }

        fun alertLevel(alertLevel: String): Builder {
            preparatory.alertLevel = alertLevel
            return this
        }

        fun build() = preparatory
    }

    class StringRequest private constructor(val value: String, val successful: Boolean) {
        companion object {
            fun createSuccessRequest(request: String): StringRequest {
                return StringRequest(request, true)
            }

            fun createFailureRequest(error: String): StringRequest {
                return StringRequest(error, false)
            }
        }

    }
}
