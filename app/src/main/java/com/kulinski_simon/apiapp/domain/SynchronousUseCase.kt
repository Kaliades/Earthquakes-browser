package com.kulinski_simon.apiapp.domain


interface SynchronousUseCase<out Result, in Params> {

    fun execute(params: Params): Result
}