package com.kulinski_simon.apiapp.domain.repository

import io.reactivex.Completable
import io.reactivex.Single

interface EarthquakeRepository<Item> {

    fun saveEarthquake(item: Item): Completable

    fun getEarthquakeById(id: String): Single<Item>

    fun getListOfAllEarthquakeFromDb(): Single<List<Item>>

    fun getListOfEarthquakeFromApi(request: String): Single<List<Item>>

    fun deleteEarthquake(id: String): Completable

}