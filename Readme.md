# Earthquakes browser

Thanks to this simple application on the Android platform you can search for earthquakes according to the given parameters.

The app allows you to view the details of earthquakes and save, them if necessary, to the database.  

I used [USGS Earthquake Api](https://earthquake.usgs.gov/fdsnws/event/1/) to get information about earthquakes. 

This project is refactorization of my old [applicaton](https://bitbucket.org/Kaliades/przegladarka-danych-z-api/src/master/).  

### Technologies
* Programming language: __Kotlin__ 
* Frameworks used: 
    * __Koin__
    * __Retrofit__ 
    * __Realm__ 
    * __RxJava 2__ 
    * __JUnit__ 
* Architecture pattern: __MVP__ 